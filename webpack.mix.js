const mix = require('laravel-mix');

// require('custom-env').env();

mix.options({
    processCssUrls: false,
    autoprefixer: {
        enabled: true,
        options: {
            browsers: ['last 2 versions', '> 1%'],
            cascade: true,
            grid: true,
        }
    }
})
    .setPublicPath('public')
    .sass('src/scss/main.scss', 'css/stylesheets.css')
    .js('src/js/app.js', 'js/scripts.compiled.js')
    .extract(['vue'])
    // .babel(['themes/youwin/assets/js/scripts.compiled.js'], 'themes/youwin/assets/js/scripts.es5.js')
    // .combine(['themes/youwin/assets/js/scripts.es5.js', 'themes/youwin/js/autocomplete.js', 'themes/youwin/js/select-autocomplete.js'], 'themes/youwin/assets/js/scripts.js')
    // .sourceMaps(true, 'source-map')
    .version();
    // .browserSync({
    //     proxy: process.env.APP_URL,
    //     files: [
    //         'themes/youwin/css/*.scss',
    //         'themes/youwin/css/components/*.scss',
    //         'themes/youwin/js/*.js'
    //     ]
    // });
