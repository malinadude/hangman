import Vue from 'vue';
import axios from 'axios';

import Paint from './components/paint';
import Keyboard from './components/keyboard';
import Win from './components/win';
import Lose from './components/lose';
import Scores from './components/scores';

Vue.component('paint', Paint);
Vue.component('keyboard', Keyboard);
Vue.component('win', Win);
Vue.component('lose', Lose);
Vue.component('scores', Scores);

new Vue({

    el: "#app",

    components: {
        Paint,
        Keyboard,
        Win,
        Lose,
        Scores,
    },

    data() {
        return {
            countries: [],
            keyboard: ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'], // вынес в app из-за присутствия не латинских букв в названиях стран
            current_word: null,
            current_word_characters: null,
            current_word_length: 0,
            success: 0,
            errors: 0,
            win: false,
            failed: false,
        }
    },

    watch: {
        errors: function (val) {
            if (val >= 11) {
                this.failed = true;

                this.setLocalStorage('losing');
                this.$refs.scores.updateScores();
            }
        },
        success: function (val) {
            if (val >= this.current_word_length) {
                this.win = true;

                this.setLocalStorage('win');
                this.$refs.scores.updateScores();
            }
        }
    },

    computed: {},

    methods: {
        get_countries() {
            axios
                .get('https://restcountries.eu/rest/v2/all')
                .then(response => {
                    if (response.data) {
                        for (let country in response.data) {
                            this.countries.push(response.data[country].name);
                        }

                        let rand = Math.floor(Math.random() * this.countries.length);

                        this.current_word = this.countries[rand];
                        this.current_word_characters = this.current_word.toUpperCase().split('');
                        this.current_word_length = this.unique(this.current_word.toUpperCase().replace(/\s+/g, '').split(''));
                    }
                }).catch(err => {
                alert(err);
            });
        },
        error_pressing() {
            this.errors += 1;
        },
        success_pressing() {
            this.success += 1;
        },
        unique(arr) {
            let result = [];

            for (let str of arr) {
                console.log(str.toUpperCase())
                if (!result.includes(str) && this.keyboard.indexOf(str.toUpperCase()) >= 0) {
                    result.push(str);
                }
            }

            console.log(result)
            return result.length;
        },
        setLocalStorage(name) {
            let type = name == 'win' ? 'Победа' : 'Проигрыш';
            let date = new Date().getDate() + '/' + new Date().getMonth() + '/' + new Date().getFullYear() + ' ' + new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
            // конкатенирую и преобразовываю в строку, чтобы шли по порядку и ключи были уникальными
            let itemName = String(new Date().getDate()) + String(new Date().getMonth()) + String(new Date().getFullYear()) + String(new Date().getHours()) + String(new Date().getMinutes()) + String(new Date().getSeconds());

            var storage = {};

            if (localStorage.getItem('scores') != null) {
                storage = JSON.parse(localStorage.getItem('scores'));

                storage[itemName] = {
                    type: type,
                    word: this.current_word,
                    date: date
                };

                localStorage.setItem('scores', JSON.stringify(storage));
            } else {
                storage[itemName] = {
                    type: type,
                    word: this.current_word,
                    date: date
                };

                localStorage.setItem('scores', JSON.stringify(storage));
            }
        },
    },

    mounted() {
        this.get_countries();
    }
});
